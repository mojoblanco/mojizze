<?php
use App\Mail\TestMail;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function() {
    return view('landing');
})->name('home');

Route::get('/about', 'PagesController@about')->name('about');


Route::get('/sendmail', function() {
    Mail::to('mojoblanco@gmail.com')->send(new TestMail());
    return 'Mail sent';
});

Route::post('/contact/send-mail', 'DefaultController@sendContactMail')->name('contact.send_mail');
