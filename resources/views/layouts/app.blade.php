<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <link rel="icon" type="image/png" href="{{ asset('img/favicon-32x32.png') }}" />

        <title>@yield('title') - {{ config('app.name') }}</title>

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <meta name="description" content="Software Developer from Lagos, Nigeria. I'm also known as mojoblanco">
        <!-- Forn Bing -->
        <meta name="msvalidate.01" content="866736EFCEB76085C69283A2FA366209" />

        <!-- Twitter Card data -->
        <meta name="twitter:card" content="summary">
        <meta name="twitter:site" content="@themojoblanco">
        <meta name="twitter:title" content="Howdy! - {{ config('app.name') }}">
        <meta name="twitter:description" content="a.k.a mojoblanco. Software Developer from Lagos, Nigeria.">
        <meta name="twitter:creator" content="@themojoblanco">
        <meta name="twitter:image" content="{{ asset('img/mojoblanco.png') }}">

        <!-- Open Graph data -->
        <meta property="og:title" content="Howdy! - {{ config('app.name') }}" />
        <meta property="og:type" content="article" />
        <meta property="og:url" content="http://www.mojizze.com/" />
        <meta property="og:image" content="{{ asset('img/mojoblanco.png') }}" />
        <meta property="og:description" content="a.k.a mojoblanco. Software Developer from Lagos, Nigeria." />
        <meta property="og:site_name" content="John Olawale" />
        <meta property="fb:admins" content="100000147983253" />

        <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->
        <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.3.2/css/mdb.min.css"> -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">

        <link rel="stylesheet" href="{{ asset('css/mojizze.css') }}">

    </head>

    <body>
        @include('shared.analyticstracking')

        @include('layouts.shared.navbar')

        <div class="container">
            <div id="page-title">
                <h1>@yield('title')</h1>
            </div>
        </div>

        <div class="container">
            <div id="content">
                @yield('content')
            </div>
        </div>


        @include('shared.contact_form')

        <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
        <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.3.2/js/mdb.min.js"></script> -->

        <script type="text/javascript">
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        </script>

        <script src="{{ asset('js/mojizze.js') }}"></script>
    </body>
</html>
