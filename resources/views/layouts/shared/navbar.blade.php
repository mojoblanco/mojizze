<nav id="navigation" class="navbar navbar-expand-lg navbar-dark red">
    <div class="container">
        <!-- Navbar brand -->
        <a class="navbar-brand" href="{{ route('home') }}">
            {{ config('app.name') }}
        </a>

        <!-- Collapse button -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
            aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>

        <!-- Collapsible content -->
        <div class="collapse navbar-collapse" id="navbarSupportedContent">

            <!-- Links -->
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('home') }}">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('about') }}">About Me</a>
                </li>
            </ul>
            <!-- Links -->

            <ul class="navbar-nav ml-auto nav-flex-icons">
                <li class="nav-item">
                    <a class="nav-link waves-effect waves-light"><i class="fa fa-twitter"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link waves-effect waves-light"><i class="fa fa-linkedin"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link waves-effect waves-light"><i class="fa fa-instagram"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link waves-effect waves-light"><i class="fa fa-skype"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link waves-effect waves-light"><i class="fa fa-github"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link waves-effect waves-light"><i class="fa fa-behance"></i></a>
                </li>
            </ul>
        </div>
        <!-- Collapsible content -->
    </div>
</nav>
