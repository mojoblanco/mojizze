@component('mail::message')
# New Contact Mail Recieved.

{{ $email['message'] }}

**Phone:** {{ $email['phone'] }}

@endcomponent
