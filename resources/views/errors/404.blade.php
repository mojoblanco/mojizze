@extends('layouts.main')

@section('title', '404')

@section('content')
    <div class="not-found text-center">
        <h1>404</h1>
        <p>Looks like what you're looking for is not here</p>
    </div>

    <div class="text-center">
        <a href="{{ route('home') }}" title="Go home" class="btn btn-cyan waves-effect waves-light">
            <i class="fa fa-home"></i> Go back home
        </a>
        <button type="button" class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#contactModal">
            <i class="fa fa-envelope"></i> Get in touch
        </button>
    </div>
@endsection
