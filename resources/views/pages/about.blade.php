@extends('layouts.app')

@section('title', 'About Me')

@section('content')
    <p class="p-large">
        Hello! My name is John Olawale a.k.a <strong>Mojoblanco</strong>.
    </p>

    <p class="p-large">
        I am a software developer from Lagos, Nigeria. I create applications for web, mobile and sometimes desktop platforms.
    </p>

    <p class="p-large">
        Sometimes I write aboute code and when I also make time to contribute to open source.
    </p>
@endsection
