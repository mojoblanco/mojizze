<!--Contact Modal -->
<div class="modal fade" id="contactModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-notify modal-info" role="document">
        <!--Content-->
        <div class="modal-content">
            <!--Header-->
            <div class="modal-header">
                <p class="heading lead">Contact Me</p>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="white-text">&times;</span>
                </button>
            </div>
            <!--Body-->
            <form action="{{ route('contact.send_mail') }}" method="post" id="contactForm">
                {{ csrf_field() }}
                
                <div class="modal-body">
                    <div class="form-group">
                        <input type="text" name="name" class="form-control"  placeholder="Full Name" required>
                    </div>
                    <div class="form-group">
                        <input type="email" name="email" class="form-control"  placeholder="Email Address" required>
                    </div>
                    <div class="form-group">
                        <input type="text" name="phone" class="form-control"  placeholder="Phone Number">
                    </div>
                    <div class="form-group">
                        <input type="text" name="subject" class="form-control" placeholder="Subject" required>
                    </div>
                    <div class="form-group">
                        <textarea name="message" placeholder="Your Message" class="md-textarea" required></textarea>
                    </div>
                </div>
                <!--Footer-->
                <div class="modal-footer justify-content-center">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">
                        <i class="fa fa-times"></i> Close
                    </button>
                    <button type="submit" class="btn btn-outline-secondary-modal waves-effect">
                        <i class="fa fa-paper-plane"></i> Send
                    </button>
                </div>
            </form>
        </div>
        <!--/.Content-->
    </div>
</div>
<!-- Contact Modal -->

