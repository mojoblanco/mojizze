@extends('layouts.main')

@section('title', 'Hello!')

@section('content')

    <div class="avatar">
        <img src="{{ asset('img/mojoblanco.png') }}" alt="mojoblanco.png" title="mojoblanco" />
    </div>

    <h1 class="name text-center animated bounceInDown">
        John Olawale <br>
    </h1>

    <p class="fadeInUp">Software Developer</p>

    <ul class="home-nav">
        <li>
            <a href="{{ route('about') }}">About Me</a>
        </li>
    </ul>

    <div class="social-icons animated bounceInUp">
        <a href="https://www.twitter.com/themojoblanco/">
            <i class="fa fa-twitter"></i>
        </a>
        <a href="https://www.facebook.com/Mojoblanco-206805739409790/">
            <i class="fa fa-facebook"></i>
        </a>
        <a href="https://www.linkedin.com/in/john-olawale-0694b245/">
            <i class="fa fa-linkedin"></i>
        </a>
        <a href="https://www.instagram.com/themojoblanco/">
            <i class="fa fa-instagram"></i>
        </a>
        <a href="skype:mojoblanco1?call">
            <i class="fa fa-skype"></i>
        </a>
        <a href="https://www.github.com/mojoblanco/">
            <i class="fa fa-github"></i>
        </a>
        <a href="https://www.behance.net/mojoblanco/">
            <i class="fa fa-behance"></i>
        </a>
    </div>

    <button type="button" class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#contactModal">
        <i class="fa fa-envelope"></i> Get in touch
    </button>

@endsection
