$(document).ready(function() {

    $('#contactForm').submit(function(event) {
    	var form = $(this);
		var url = form.attr('action');
		var data = form.serializeArray();
		var submitBtn = form.find(':submit');

		submitBtn.html('Sending...');
		submitBtn.prop('disabled', true);

		$.ajax({
			type: 'POST',
			url: url,
			data: data,
			dataType: 'text',
			success: function(result) {
				alert("Email Sent");
			},
			error: function() {
				alert("Error sending email");
			},
			complete: function() {
				submitBtn.html('<i class="fa fa-envelope"></i> Get in touch');
				submitBtn.prop('disabled', false);
			}
		});

		event.preventDefault();
	});

});
