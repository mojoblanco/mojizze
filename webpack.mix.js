let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/assets/js/app.js', 'public/js')
//    .sass('resources/assets/sass/app.scss', 'public/css');
//


mix.copyDirectory('node_modules/mdbootstrap/font', 'public/font');
// mix.copyDirectory('node_modules/font-awesome/fonts', 'public/fonts');


mix.styles([
    'node_modules/mdbootstrap/css/bootstrap.css',
    // 'node_modules/font-awesome/css/font-awesome.css',
    'node_modules/mdbootstrap/css/mdb.css',
    // 'resources/assets/css/animate.css',
    'resources/assets/css/styles.css',
], 'public/css/mojizze.css');

mix.scripts([
    // 'node_modules/mdbootstrap/js/jquery-2.2.3.js',
    'node_modules/mdbootstrap/js/tether.js',
    'node_modules/mdbootstrap/js/bootstrap.js',
    'node_modules/mdbootstrap/js/mdb.js',
    'resources/assets/js/main.js',
], 'public/js/mojizze.js');
