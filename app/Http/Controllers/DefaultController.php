<?php

namespace App\Http\Controllers;

use Mail;
use App\Mail\ContactMail;
use Illuminate\Http\Request;

class DefaultController extends Controller
{
    public function sendContactMail(Request $req)
    {
        $rd = $req->all();
        try {
            Mail::to('hi@mojizze.com')->send(new ContactMail($rd));
        } catch (Exception $e) {

        }

        return 'success';
    }
}
